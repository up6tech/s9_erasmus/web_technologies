FROM debian:bullseye AS builder

# Move requirements
RUN mkdir -p /build/client
WORKDIR /build/client
# Move package json
COPY client/package.json /build/client/package.json
COPY client/package-lock.json /build/client/package-lock.json
RUN apt update -y && apt install nodejs npm -y
# Install package json
RUN npm install
# Compile react app
COPY client/ /build/client/
RUN npm run build

FROM python:3.10.8-slim-bullseye AS runner

# Copy python app (source and .venv)
RUN mkdir -p /build/server
WORKDIR /build
# Copy react app (build result)
COPY --from=builder /build/client/build /build/server/build
# Copy python project
COPY server/ /build/server
COPY requirements.txt /build/requirements.txt
RUN pip install -r requirements.txt
EXPOSE 8080
# execute command
WORKDIR /build/server
RUN python manage.py migrate
ENTRYPOINT [ "python", "manage.py", "runserver", "8080" ]
# CMD sh -c '. .venv/bin/activate && django-admin --version && cd server && python manage.py runserver 8080'
