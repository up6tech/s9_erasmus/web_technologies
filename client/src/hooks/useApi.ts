import { useEffect, useState } from "react";
type ApiCallback = (data: any) => void;

export function useApi<T>(
  url: string,
  effect?: ApiCallback | undefined
): [boolean, boolean, T] {
  // Loading state
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  // Error state
  const [isError, setError] = useState<boolean>(false);
  // API data state
  const [item, setItem] = useState<T>();

  // update the data when url is updated or on the first mount of the component
  useEffect(() => {
    let isMounted = true;
    fetch("http://localhost:8080" + url)
      .then((res) => res.json())
      .then((data) => {
        if (isMounted) {
          setIsLoaded(true);
          if (data.error) {
            setError(true);
          }
          // Donnée chargée
          setItem(data);

          if (effect) effect(data);
        }
      })
      .catch((error) => {
        if (isMounted) {
          // Erreur sur la requête
          setIsLoaded(true);
          setError(true);
          if (effect) effect({});
        }
      });
    return () => {
      isMounted = false;
    };
  }, [url]);
  return [isLoaded, isError, item as T];
}

/*// Etat de chargement
	const [isLoaded, setIsLoaded] = useState<boolean>(false)
	// Etat d'erreur
	const [isError, setError] = useState<boolean>(false)
	// Etat des données utilisateur
	const [item, setItem] = useState<T>()

	// Méthode qui s'active sur un changement de variable
	useEffect(() => {
		let isMounted = true

		fetch(GlobalConf.apiBaseUrl + url)
			.then((res) => res.json())
			.then((data) => {
				if (isMounted) {
					setIsLoaded(true)
					if (data.error) {
						setError(true)
					}
					// Donnée chargée
					setItem(data)

					if (effect) effect(data)
				}
			})
			.catch((error) => {
				if (isMounted) {
					// Erreur sur la requête
					setIsLoaded(true)
					setError(true)
					if (effect) effect({})
				}
			})
		return () => {
			isMounted = false
		}
	}, [url])
	return [isLoaded, isError, item]
*/
