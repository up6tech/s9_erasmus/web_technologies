import React, { FC } from "react";
import { createContext, useContext, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { DEFAULT_VALUE, ShopItemStruct } from "src/components/Types";
import { useLocalStorage } from "src/hooks/useLocalStorage";

interface Props {
  children?: React.ReactNode;
}

export interface User {
  token: string;
  username: string;
  email: string;
  basket: ShopItemStruct[];
}

export interface AuthData {
  user: User | string;
  isAuth: () => boolean;
  login: (data: User) => Promise<void>;
  updateUser: (data: User) => Promise<void>;
  logout: () => void;
}

const AuthContext = createContext<AuthData>({
  user: DEFAULT_VALUE,
  isAuth: () => false,
  login: async (_) => {},
  updateUser: async (_) => {},
  logout: () => {},
});

export const AuthProvider: FC<Props> = ({ children }) => {
  // localstorage
  const [user, setUser] = useLocalStorage<User | string>("user", DEFAULT_VALUE);
  const navigate = useNavigate();

  const login = async (data: User) => {
    updateUser(data);
    navigate("/shop", { replace: true });
  };

  const updateUser = async (data: User) => {
    setUser(data);
  };

  const logout = () => {
    setUser(DEFAULT_VALUE);
    navigate("/shop", { replace: true });
  };

  const isAuth = () => {
    return user !== DEFAULT_VALUE;
  };

  const value: AuthData = useMemo<AuthData>(
    () => ({
      user,
      isAuth,
      login,
      logout,
      updateUser,
    }),
    [user]
  );
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => {
  return useContext(AuthContext);
};
