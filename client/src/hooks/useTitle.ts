import { useEffect } from "react";

/**
 *
 * @param title The title of the page
 */
export function useTitle(title: string) {
  useEffect(() => {
    const prevTitle = document.title;
    document.title = title;
    return () => {
      document.title = prevTitle;
    };
  }, [title]);
}
