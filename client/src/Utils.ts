export function capitalize(string: string): string {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 *
 * @param num The number to round
 * @returns the number rounded
 */
export function round2(num: number): number {
  return Math.round(num * 100 + Number.EPSILON) / 100;
}

/**
 *
 * @param size Size of generated array
 * @param startAt start at a specified value
 * @returns
 */
export function range(size: number, startAt: number = 0): number[] {
  return [...Array(size).keys()].map((i) => i + startAt);
}
