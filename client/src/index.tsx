import React from "react";
import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import "./index.css";
import { AuthLayout } from "./layout/AuthLayout";
import { ChangePassword } from "./pages/ChangePassword";
import ErrorPage from "./pages/Error";
import { MyItems } from "./pages/MyItem";
import { ShopIndex } from "./pages/ShopIndex";
import { Signin } from "./pages/Signin";
import { Signup } from "./pages/Signup";
import { UiKit } from "./pages/UiKit";
import reportWebVitals from "./reportWebVitals";
import {
  CheckAuthentification,
  CheckNonAuthentification,
} from "./routes/Authenticated";
const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route element={<AuthLayout />} errorElement={<ErrorPage />}>
      <Route
        path="/shop"
        element={<ShopIndex />}
        errorElement={<ErrorPage />}
      />
      <Route
        path="/shop/:page"
        element={<ShopIndex />}
        errorElement={<ErrorPage />}
      />
      <Route path="/uikit" element={<UiKit />} errorElement={<ErrorPage />} />
      <Route
        path="/myitems"
        element={
          <CheckAuthentification>
            <MyItems />
          </CheckAuthentification>
        }
        errorElement={<ErrorPage />}
      />
      <Route
        path="/signup"
        element={
          <CheckNonAuthentification>
            <Signup />
          </CheckNonAuthentification>
        }
      />
      <Route
        path="/login"
        element={
          <CheckNonAuthentification>
            <Signin />
          </CheckNonAuthentification>
        }
      />
      <Route
        path="/account"
        element={
          <CheckAuthentification>
            <ChangePassword />
          </CheckAuthentification>
        }
      />
    </Route>
  )
);

root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
