import { FC } from "react";
import { Container } from "src/components/html/Container";
import { Footer } from "src/components/html/Footer";
import { Header } from "src/components/html/Header";
import { Paper } from "src/components/html/Paper";
import { SignupForm } from "src/components/SignupForm";
import { useTitle } from "src/hooks/useTitle";
import style from "src/style/Signup.module.css";

export const Signup: FC = (props) => {
  useTitle("Sign up");
  return (
    <div>
      <Header />
      <Container
        style={{
          marginTop: "25px",
          marginBottom: "25px",
        }}
      >
        <section className={style.main}>
          <Paper>
            <h1>Sign up</h1>
            <SignupForm />
          </Paper>
        </section>
      </Container>
      <Footer />
    </div>
  );
};
