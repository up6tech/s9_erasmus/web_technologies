import { FC } from "react";
import { Container } from "src/components/html/Container";
import { Footer } from "src/components/html/Footer";
import { Header } from "src/components/html/Header";
import { Paper } from "src/components/html/Paper";
import { SigninForm } from "src/components/SigninForm";
import { useTitle } from "src/hooks/useTitle";
import style from "src/style/Signin.module.css";

export const Signin: FC = (props) => {
  useTitle("Sign in");
  return (
    <div>
      <Header />
      <Container
        style={{
          marginTop: "25px",
          marginBottom: "25px",
        }}
      >
        <section className={style.main}>
          <Paper>
            <h1>Sign in</h1>
            <SigninForm />
          </Paper>
        </section>
      </Container>
      <Footer />
    </div>
  );
};
