import { FC, useState } from "react";
import { Button } from "src/components/html/Button";
import { Container } from "src/components/html/Container";
import { Footer } from "src/components/html/Footer";
import { Header } from "src/components/html/Header";
import { Modal } from "src/components/html/Modal";
import { Alert } from "src/components/indicators/Alert";
import { Skeleton } from "src/components/indicators/Skeleton";
import { useTitle } from "src/hooks/useTitle";

export const UiKit: FC = (props) => {
  useTitle("UI Kit - Components Demo");
  const [open, setOpen] = useState<boolean>(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Header />
      <Container>
        <Alert type={"success"}>Operation executed successfuly</Alert>
        <Alert type={"error"}>Operation failed</Alert>
        <Alert type={"warning"}>Operation cannot be done</Alert>
        <Skeleton height={"300px"} />
        <Button onClick={handleOpen}>Show modal</Button>
        <Modal open={open}>
          <h1>Hello modal !</h1>
          <Button onClick={handleClose}>Close modal</Button>
        </Modal>
      </Container>
      <Footer />
    </>
  );
};
