import { FC } from "react";
import { Container } from "src/components/html/Container";
import { Footer } from "src/components/html/Footer";
import { Header } from "src/components/html/Header";
import { MyShopItems } from "src/components/MyShopItems";
import { useTitle } from "src/hooks/useTitle";
import style from "src/style/ShopIndex.module.css";

export const MyItems: FC = (props) => {
  useTitle("Shop - My Items");
  return (
    <div>
      <Header enableSearch={false} enableBasket={false} />
      <Container
        style={{
          marginTop: "25px",
          marginBottom: "25px",
        }}
      >
        <section className={style.main}>
          <MyShopItems />
        </section>
      </Container>
      <Footer />
    </div>
  );
};
