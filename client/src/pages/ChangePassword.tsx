import { FC } from "react";
import { ChangePasswordForm } from "src/components/ChangePasswordForm";
import { Container } from "src/components/html/Container";
import { Footer } from "src/components/html/Footer";
import { Header } from "src/components/html/Header";
import { Paper } from "src/components/html/Paper";
import { useTitle } from "src/hooks/useTitle";
import style from "src/style/Signin.module.css";

export const ChangePassword: FC = (props) => {
  useTitle("Shop - Change password");
  return (
    <div>
      <Header enableBasket={false} />
      <Container
        style={{
          marginTop: "25px",
          marginBottom: "25px",
        }}
      >
        <section className={style.main}>
          <Paper>
            <h1>Change Password</h1>
            <ChangePasswordForm />
          </Paper>
        </section>
      </Container>
      <Footer />
    </div>
  );
};
