import { FC, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Cart } from "src/components/Cart";
import { Container } from "src/components/html/Container";
import { Footer } from "src/components/html/Footer";
import { Header } from "src/components/html/Header";
import { Modal } from "src/components/html/Modal";
import { ShopItems } from "src/components/ShopItems";
import { useTitle } from "src/hooks/useTitle";
import style from "src/style/ShopIndex.module.css";

export const ShopIndex: FC = (props) => {
  useTitle("Shop - Items");
  const pageParams = useParams();
  const [filter, setFilter] = useState<string>("");
  const [cartOpen, setCartOpen] = useState<boolean>(false);
  const navigate = useNavigate();

  const handleOpen = () => setCartOpen(true);
  const handleClose = (forceUpdate: boolean) => {
    setCartOpen(false);
    if (forceUpdate) navigate(0);
  };

  return (
    <div>
      <Header
        enableSearch={true}
        openBasket={handleOpen}
        updateSearch={(inputStr) => setFilter(inputStr)}
      />
      <Container
        style={{
          marginTop: "25px",
          marginBottom: "25px",
        }}
      >
        <section className={style.main}>
          <ShopItems
            filter={filter}
            page={parseInt(pageParams.page ?? "1") - 1}
          />
        </section>
      </Container>
      <Modal open={cartOpen}>
        <Cart onClose={handleClose} />
      </Modal>
      <Footer />
    </div>
  );
};
