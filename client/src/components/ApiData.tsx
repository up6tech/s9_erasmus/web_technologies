import { FC } from "react";
import { useApi } from "src/hooks/useApi";
import { Alert } from "./indicators/Alert";
import { Skeleton } from "./indicators/Skeleton";

interface Props<T> {
  url: string;
  render: (data: T) => JSX.Element;
  height?: number;
}

const ApiData: FC<Props<any>> = <T,>(props: Props<T>) => {
  const [isLoaded, isError, item] = useApi<T>(props.url);
  if (isLoaded && isError) {
    return (
      <>
        <Alert type="error">An unknown error happened</Alert>
      </>
    );
  }
  if (!isLoaded || !item) {
    return (
      <>
        <Skeleton width="100%" height={props.height || 300} />
      </>
    );
  }

  return props.render(item);
};
ApiData.defaultProps = {};
export { ApiData };
