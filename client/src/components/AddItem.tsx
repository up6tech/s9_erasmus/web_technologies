import { FC, useRef, useState } from "react";
import { Paper } from "./html/Paper";
import style from "src/style/components/AddItem.module.css";
import { Stack } from "./html/Stack";
import { Button } from "./html/Button";
import { useAuth, User } from "src/hooks/useAuth";
import { Alert } from "./indicators/Alert";
import { DEFAULT_VALUE } from "./Types";

interface Props {
  onSubmit: () => Promise<void>;
}

const url = "http://localhost:8080/api/user/item";

export const AddItem: FC<Props> = (props) => {
  const { user } = useAuth();
  const [locked, setLocked] = useState<boolean>(false);
  const [error, setError] = useState<string>(DEFAULT_VALUE);
  const name = useRef<HTMLInputElement>(null);
  const price = useRef<HTMLInputElement>(null);
  const description = useRef<HTMLTextAreaElement>(null);

  const handleSubmit = async () => {
    setLocked(true);
    setError(DEFAULT_VALUE);

    const nameVal = name.current?.value || DEFAULT_VALUE;
    const priceVal = parseFloat(price.current?.value || "-1");
    const descriptionVal = description.current?.value || DEFAULT_VALUE;

    if (nameVal === DEFAULT_VALUE) {
      setError("A valid name must be specified");
      setLocked(false);
      return;
    }

    if (priceVal <= 0) {
      setError("A valid price must be specified");
      setLocked(false);
      return;
    }

    if (descriptionVal === DEFAULT_VALUE) {
      setError("A valid description must be specified");
      setLocked(false);
      return;
    }

    const data = JSON.stringify({
      name: nameVal,
      price: priceVal,
      description: descriptionVal,
    });

    const res = await fetch(url, {
      method: "POST",
      body: data,
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + (user as User).token,
      },
    });
    if (res.status == 200) {
      const data = await res.json();
      await props.onSubmit?.();
    } else {
      const data = res.statusText;
      setError(data);
    }
    setLocked(false);
  };

  return (
    <Paper className={style.paperContainer}>
      {error !== DEFAULT_VALUE && <Alert type={"error"}>{error}</Alert>}
      <div className={style.container}>
        <Stack>
          <label htmlFor="name">Name</label>
          <label htmlFor="price">Price</label>
          <label htmlFor="description">Description</label>
        </Stack>
        <Stack>
          <input required ref={name} type={"text"} id="name" />
          <input required ref={price} type={"number"} id="price" />
          <textarea
            ref={description}
            id="desription"
            required
            rows={6}
            cols={70}
            style={{ resize: "none" }}
          />
        </Stack>
      </div>
      <Button disabled={locked} onClick={handleSubmit}>
        Submit item
      </Button>
    </Paper>
  );
};
