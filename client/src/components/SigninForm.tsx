import { FC, useRef, useState } from "react";
import { Button } from "src/components/html/Button";
import { Stack } from "src/components/html/Stack";
import { useAuth } from "src/hooks/useAuth";
import style from "src/style/components/SigninForm.module.css";
import { Alert } from "./indicators/Alert";
import { DEFAULT_VALUE } from "./Types";

interface Props {}

const url = "http://localhost:8080/api/auth/signin";
const userinfoUrl = "http://localhost:8080/api/auth/userinfo";

export const SigninForm: FC<Props> = (props) => {
  // form
  const email = useRef<HTMLInputElement>(null);
  const password = useRef<HTMLInputElement>(null);

  const [error, setError] = useState<string>(DEFAULT_VALUE);
  const [info, setInfo] = useState<string>(DEFAULT_VALUE);
  const [locked, setLocked] = useState<boolean>(false);
  // auth
  const { login } = useAuth();

  const onSubmit: React.MouseEventHandler<HTMLAnchorElement> = (event) => {
    // handle submit here
    setInfo(DEFAULT_VALUE);
    setError(DEFAULT_VALUE);
    event.preventDefault();
    setLocked(true);
    const emailValue = email.current?.value;
    const passwordValue = password.current?.value;

    const data = JSON.stringify({
      email: emailValue as string,
      password: passwordValue as string,
    });
    fetch(url, {
      method: "POST",
      body: data,
      headers: { "Content-Type": "application/json" },
      redirect: "follow",
    }).then(async (res) => {
      if (res.status !== 200) {
        setError("Invalid credentials");
        setLocked(false);
      } else {
        const { token } = (await res.json()) as any;
        const userinfoRes = await fetch(userinfoUrl, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Token " + token,
          },
          redirect: "follow",
        });
        const { email, username } = await userinfoRes.json();
        setLocked(false);
        await login({
          email: email,
          username: username,
          token: token,
          basket: [],
        });
      }
    });
  };

  return (
    <div>
      <form>
        {error != DEFAULT_VALUE && <Alert type="error">{error}</Alert>}
        {info != DEFAULT_VALUE && <Alert type="success">{info}</Alert>}
        <div className={style.grid}>
          <Stack style={{ gap: "15px" }}>
            <label htmlFor="email">Email</label>
            <label htmlFor="password">Password</label>
          </Stack>
          <Stack>
            <input
              ref={email}
              id="email"
              placeholder="john.doe@email.com"
              type={"email"}
            />
            <input ref={password} id="password" type={"password"} />
          </Stack>
        </div>
        <Stack className={style.actions}>
          <Button onClick={onSubmit} disabled={locked}>
            Sign in
          </Button>
        </Stack>
      </form>
    </div>
  );
};
