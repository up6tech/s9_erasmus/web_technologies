import { FC, useRef, useState } from "react";
import style from "src/style/components/SigninForm.module.css";
import { Button } from "src/components/html/Button";
import { Stack } from "src/components/html/Stack";
import { useAuth, User } from "src/hooks/useAuth";
import { Alert } from "./indicators/Alert";
import { DEFAULT_VALUE } from "./Types";

const url = "http://localhost:8080/api/auth/changepassword";

interface Props {}

export const ChangePasswordForm: FC<Props> = (props) => {
  const oldPassword = useRef<HTMLInputElement>(null);
  const password = useRef<HTMLInputElement>(null);
  const [locked, setLocked] = useState<boolean>(false);
  const [error, setError] = useState<string>(DEFAULT_VALUE);
  const [info, setInfo] = useState<string>(DEFAULT_VALUE);
  const { isAuth, user } = useAuth();

  const onSubmit: React.MouseEventHandler<HTMLAnchorElement> = async (
    event
  ) => {
    setError(DEFAULT_VALUE);
    setInfo(DEFAULT_VALUE);
    setLocked(true);
    // handle submit here
    event.preventDefault();
    const oldPasswordValue = oldPassword.current?.value;
    const passwordValue = password.current?.value;

    const data = JSON.stringify({
      oldPassword: oldPasswordValue,
      password: passwordValue,
    });

    const res = await fetch(url, {
      method: "POST",
      body: data,
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + (user as User).token,
      },
    });

    switch (res.status) {
      case 200:
        const success = await res.json();
        setInfo(success.ok);
        break;
      default:
        const error = await res.json();
        setError(error.error);
        break;
    }
    setLocked(false);
  };

  return (
    <div>
      <form>
        {error != DEFAULT_VALUE && <Alert type="error">{error}</Alert>}
        {info != DEFAULT_VALUE && <Alert type="success">{info}</Alert>}
        <div className={style.grid}>
          <Stack style={{ gap: "15px" }}>
            <label htmlFor="oldPassword">Old password</label>
            <label htmlFor="password">New Password</label>
          </Stack>
          <Stack>
            <input ref={oldPassword} id="oldPassword" type={"password"} />
            <input ref={password} id="password" type={"password"} />
          </Stack>
        </div>
        <Stack className={style.actions}>
          <Button onClick={onSubmit} disabled={locked}>
            Change password
          </Button>
        </Stack>
      </form>
    </div>
  );
};
