export const DEFAULT_VALUE = "__none__";

export interface ShopItemsResponseStruct {
  /**
   * Total number of product in database
   */
  totalProducts: number;
  /**
   * Product returned by the query
   */
  products: ShopItemStruct[];

  /**
   * The number of product displayed on a single page
   */
  productPerPage: number;
}

export interface ShopItemStruct {
  id: string;
  name: string;
  description: string;
  image_file_name: string;
  price: number;
  creation: string;
  last_update: string;
  owner: string;
  buyer: string;
}

export const NONE_PRODUCT: ShopItemStruct = {
  id: DEFAULT_VALUE,
  name: DEFAULT_VALUE,
  description: DEFAULT_VALUE,
  image_file_name: DEFAULT_VALUE,
  price: -1,
  creation: DEFAULT_VALUE,
  last_update: DEFAULT_VALUE,
  owner: DEFAULT_VALUE,
  buyer: DEFAULT_VALUE,
};
