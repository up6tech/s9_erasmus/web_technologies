import moment from "moment";
import { FC, useState } from "react";
import { Paper } from "src/components/html/Paper";
import { useAuth, User } from "src/hooks/useAuth";
import style from "src/style/components/InteractiveShopItem.module.css";
import { Button } from "./html/Button";

interface Props {
  id: string;
  name: string;
  description: string;
  price: number;
  owner: string;
  image: string;
  creation: string;
  deletable?: boolean;
  onDelete?: () => Promise<void>;
}

const url = "http://localhost:8080/api/user/item";

export const InteractiveShopItem: FC<Props> = (props) => {
  const { user } = useAuth();
  const [locked, setLocked] = useState<boolean>(false);
  const isDeletable = props.deletable ?? false;

  const handleDelete = async () => {
    setLocked(true);
    const res = await fetch(url, {
      method: "DELETE",
      body: JSON.stringify({ id: props.id }),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + (user as User).token,
      },
    });
    if (res.status == 200) {
      await props.onDelete?.();
    } else {
      console.error(res.status + "/" + res.statusText);
    }
    setLocked(false);
  };

  return (
    <Paper>
      <div className={style.shopItem}>
        <div className={style.shopItemLeft}>
          <p>
            <strong>Name:</strong> {props.name}
          </p>
          <p>
            <strong>Description:</strong> {props.description}
          </p>
          <p>
            <strong>Price:</strong> {props.price}
          </p>
          <p>
            <strong>Added by</strong> {props.owner}{" "}
            {moment(props.creation).fromNow()}
          </p>
        </div>
        <div className={style.shopItemRight}>
          <div
            className={style.shopItemImage}
            style={{
              backgroundImage: `url(http://localhost:8080/static/${props.image})`,
            }}
          />
        </div>
      </div>
      {isDeletable && (
        <Button disabled={locked} onClick={handleDelete}>
          Delete
        </Button>
      )}
    </Paper>
  );
};
