import { FC } from "react";
import style from "src/style/components/Alert.module.css";
import { capitalize } from "src/Utils";

type AlertType = "error" | "warning" | "success";
interface Props {
  type: AlertType;
  children?: React.ReactNode;
}

const Alert: FC<Props> = (props) => {
  return (
    <div className={`${style.alert} ${style[props.type]}`}>
      <span className={style.title}>{capitalize(props.type)}</span>
      <span className={style.desc}>{props.children}</span>
    </div>
  );
};

export { Alert };
