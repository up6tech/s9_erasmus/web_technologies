import { FC } from "react";
import style from "src/style/components/Skeleton.module.css";

interface Props {
  width?: string | number;
  height?: string | number;
}

const Skeleton: FC<Props> = (props) => {
  return (
    <div
      className={style.container}
      style={{
        width: props.width || "100%",
        height: props.height || 300,
      }}
    ></div>
  );
};

export { Skeleton };
