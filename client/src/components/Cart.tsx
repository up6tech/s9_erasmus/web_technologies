import { FC, useState } from "react";
import { useAuth, User } from "src/hooks/useAuth";
import { DEFAULT_VALUE, ShopItemStruct } from "./Types";
import style from "src/style/components/Cart.module.css";
import { Button } from "./html/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashCan } from "@fortawesome/free-solid-svg-icons";
import { Alert } from "./indicators/Alert";
import { round2 } from "src/Utils";

interface Props {
  onClose: (forceUpdate: boolean) => void;
}

export const Cart: FC<Props> = (props) => {
  const [force, setForce] = useState<boolean>(false);
  const { isAuth, user, updateUser } = useAuth();
  const [locked, setLocked] = useState<boolean>(false);
  const [info, setInfo] = useState<string>(DEFAULT_VALUE);
  const [error, setError] = useState<string>(DEFAULT_VALUE);
  const [deleted, setDeleted] = useState<string[]>([]);
  const [updated, setUpdated] = useState<ShopItemStruct[]>([]);

  let cart: ShopItemStruct[] = [];
  if (user !== DEFAULT_VALUE) {
    cart = (user as User).basket;
  }

  const hasUpdate = deleted.length > 0 || updated.length > 0;

  const handleEmptyBasket = () => {
    const userData = user as User;
    userData.basket = [];
    updateUser(userData);
  };

  const handleRemoveBasketById = (user: User, itemId: string) => {
    let removeIndex = -1;
    for (let index = 0; index < user.basket.length; index++) {
      const element = user.basket[index];
      if (element.id == itemId) {
        removeIndex = index;
        break;
      }
    }
    if (removeIndex >= 0) user.basket.splice(removeIndex, 1);
  };

  const handleRemoveBasket = (index: number) => {
    const userData = user as User;
    userData.basket.splice(index, 1);
    updateUser(userData);
  };

  const includeUpdate = (index: string) => {
    return updated.filter((item) => item.id == index).length != 0;
  };

  const getUpdated = (index: string): ShopItemStruct => {
    return updated.filter((item) => item.id == index)[0];
  };

  const handlePay = async () => {
    setLocked(true);
    // check for update
    if (hasUpdate) {
      const userData = user as User;
      for (const deletedItemId of deleted) {
        handleRemoveBasketById(userData, deletedItemId);
      }
      for (const updatedItem of updated) {
        for (const cartItem of userData.basket) {
          if (cartItem.id == updatedItem.id) {
            cartItem.name = updatedItem.name;
            cartItem.price = updatedItem.price;
          }
        }
      }

      updateUser(userData);
      setDeleted([]);
      setUpdated([]);
      setLocked(false);
      setInfo(DEFAULT_VALUE);
      setError(DEFAULT_VALUE);
      return;
    }
    const paymentUrl = "http://localhost:8080/api/user/payment";
    const itemInfoUrl = "http://localhost:8080/api/item/";
    // check for validity
    let allValid = true;
    const updatedItems: ShopItemStruct[] = []; // indeces
    const deletedItems: string[] = []; // indeces
    const userData = user as User;

    for (let index = 0; index < userData.basket.length; index++) {
      const item = userData.basket[index];
      const res = await fetch(itemInfoUrl + item.id);
      if (res.status == 200) {
        // compare price
        const json: ShopItemStruct = await res.json();
        if (json.price !== item.price) {
          allValid = false;
          updatedItems.push(json);
        }
      } else if (res.status == 404) {
        // mark as deleted
        allValid = false;
        deletedItems.push(item.id);
      } else {
        console.error(await res.text());
      }
    }
    if (!allValid) {
      // print errors
      setUpdated(updatedItems);
      setDeleted(deletedItems);
      setError(
        "A change occured between cart and purchase," +
          "deleted items will be removed automaticly from the basket." +
          "updated items will stay on the cart, unless you remove them." +
          "click 'Pay' to continue"
      );
      setLocked(false);
      return;
    }
    const productIds = userData.basket.map((item) => parseInt(item.id));
    const data = JSON.stringify({ items: productIds });
    const paymentRes = await fetch(paymentUrl, {
      method: "POST",
      body: data,
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + (user as User).token,
      },
    });
    if (paymentRes.status == 200) {
      setInfo("Payment validated ! an email was sent to you !");
      handleEmptyBasket();
    } else {
      const json = await paymentRes.json();
      if (json.error) {
        setError(json.error);
      } else if (json.detail) {
        setError(json.detail);
      }
    }
    setForce(true);
    setLocked(false);
  };

  const isEmpty = cart.length == 0;
  const totalCartCost = round2(
    cart.reduce(
      (accumulator, current) =>
        accumulator + parseFloat(current.price as any as string),
      0
    )
  );

  return (
    <>
      {error != DEFAULT_VALUE && <Alert type="error">{error}</Alert>}
      {info != DEFAULT_VALUE && <Alert type="success">{info}</Alert>}
      <h1>Current cart</h1>
      {isEmpty && <p>Cart empty</p>}
      {!isEmpty && (
        <table className={style.table}>
          <thead>
            <tr className={style.tr}>
              <th className={style.th}>Item</th>
              <th className={style.th}>Price</th>
              <th className={`${style.th} ${style.delete}`}>
                <Button
                  onClick={handleEmptyBasket}
                  style={{ backgroundColor: "#ff415c" }}
                >
                  Remove all
                </Button>
              </th>
            </tr>
          </thead>
          <tbody>
            {cart.map((value, index) => {
              const isDeleted = deleted.includes(value.id);
              const isUpdated = includeUpdate(value.id);
              return (
                <tr className={style.tr} key={index}>
                  <td className={style.td}>{value.name}</td>
                  <td className={style.td}>
                    {isUpdated ? (
                      <>
                        Old price: {value.price}€, New price:
                        {getUpdated(value.id).price}€
                      </>
                    ) : (
                      <>{value.price}€</>
                    )}
                  </td>
                  <td className={`${style.td} ${style.delete}`}>
                    {isDeleted && <p>this item has been sold</p>}
                    {!isDeleted && (
                      <Button
                        onClick={() => handleRemoveBasket(index)}
                        style={{ backgroundColor: "#ff415c" }}
                      >
                        <FontAwesomeIcon icon={faTrashCan} />
                      </Button>
                    )}
                  </td>
                </tr>
              );
            })}
            <tr className={`${style.tr} ${style.footer}`}>
              <td className={style.td}>Total</td>
              <td className={`${style.td}`} colSpan={2}>
                {totalCartCost}€
              </td>
            </tr>
          </tbody>
        </table>
      )}
      <Button onClick={handlePay} disabled={locked}>
        {hasUpdate ? <>Update cart</> : <>Pay</>}
      </Button>
      <Button
        onClick={() => {
          props.onClose(force);
          setForce(false);
        }}
      >
        Close
      </Button>
    </>
  );
};
