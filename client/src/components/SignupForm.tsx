import { FC, useRef, useState } from "react";
import style from "src/style/components/SignupForm.module.css";
import { Button } from "src/components/html/Button";
import { Stack } from "src/components/html/Stack";
import { DEFAULT_VALUE } from "./Types";
import { Alert } from "./indicators/Alert";

interface Props {}

const url = "http://localhost:8080/api/auth/signup";

export const SignupForm: FC<Props> = (props) => {
  const email = useRef<HTMLInputElement>(null);
  const username = useRef<HTMLInputElement>(null);
  const password = useRef<HTMLInputElement>(null);
  const confirmPassword = useRef<HTMLInputElement>(null);
  const [error, setError] = useState<string>(DEFAULT_VALUE);
  const [info, setInfo] = useState<string>(DEFAULT_VALUE);

  const onReset: React.MouseEventHandler<HTMLAnchorElement> = (event) => {
    setInfo(DEFAULT_VALUE);
    setError(DEFAULT_VALUE);
  };

  const onSubmit: React.MouseEventHandler<HTMLAnchorElement> = (event) => {
    setInfo(DEFAULT_VALUE);
    setError(DEFAULT_VALUE);
    // handle submit here
    event.preventDefault();
    const emailValue = email.current?.value;
    const usernameValue = username.current?.value;
    const passwordValue = password.current?.value;
    const confirmPasswordValue = confirmPassword.current?.value;

    if (passwordValue != confirmPasswordValue) {
      setError("Password do not match");
      return;
    }

    const data = JSON.stringify({
      email: emailValue as string,
      username: usernameValue as string,
      password: passwordValue as string,
    });

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: data,
    }).then(async (res) => {
      const data = await res.json();
      if (res.status != 200) {
        setError(data.error);
      } else {
        setInfo("Registered successfuly ! use 'Signin' to login");
      }
    });
  };

  return (
    <div>
      <form>
        {error != DEFAULT_VALUE && <Alert type="error">{error}</Alert>}
        {info != DEFAULT_VALUE && <Alert type="success">{info}</Alert>}
        <div className={style.grid}>
          <Stack style={{ gap: "15px" }}>
            <label htmlFor="email">Email</label>
            <label htmlFor="username">Username</label>
            <label htmlFor="password">Password</label>
            <label htmlFor="cpassword">Confirm password</label>
          </Stack>
          <Stack>
            <input
              ref={email}
              id="email"
              placeholder="john.doe@email.com"
              type={"email"}
            />
            <input
              ref={username}
              id="username"
              placeholder="johndoe"
              type={"text"}
            />
            <input ref={password} id="password" type={"password"} />
            <input ref={confirmPassword} id="cpassword" type={"password"} />
          </Stack>
        </div>
        <Stack className={style.actions}>
          <Button onClick={onReset}>Reset</Button>
          <Button onClick={onSubmit}>Sign up</Button>
        </Stack>
      </form>
    </div>
  );
};
