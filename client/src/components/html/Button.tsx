import { FC } from "react";
import { Link } from "react-router-dom";
import style from "src/style/components/Button.module.css";

interface Props {
  children: React.ReactNode;
  href?: string;
  style?: React.CSSProperties | undefined;
  className?: string;
  disabled?: boolean;
  onClick?: React.MouseEventHandler<HTMLAnchorElement>;
}

export const Button: FC<Props> = (props) => {
  const isDisabled = props.disabled ?? false;
  return (
    <Link
      className={`${style.button} ${props?.className} ${
        isDisabled ? style.disabled : style.enabled
      }`}
      style={{
        ...props.style,
        ...{
          cursor: isDisabled ? "not-allowed" : "pointer",
        },
      }}
      to={isDisabled ? "#" : props.href || "#"}
      onClick={isDisabled ? () => {} : props.onClick}
    >
      {props.children}
    </Link>
  );
};
