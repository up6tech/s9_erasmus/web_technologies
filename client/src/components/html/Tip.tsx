import { FC, ReactNode } from "react";
import style from "src/style/components/Tip.module.css";

interface Props {
  tag: string | JSX.Element;
  children?: ReactNode;
  disabled?: boolean;
}

export const Tip: FC<Props> = (props) => {
  const isDisabled = props.disabled ?? false;
  return (
    <div className={style.tip}>
      {!isDisabled && <div className={style.tipContainer}>{props.tag}</div>}
      <div className={style.content}>{props.children}</div>
    </div>
  );
};
