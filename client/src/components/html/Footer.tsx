import { FC } from "react";
import style from "src/style/components/Footer.module.css";
import { Container } from "./Container";

export const Footer: FC = (props) => {
  return (
    <>
      <footer className={style.footer}>
        <Container className={style.container}>
          <span className={style.info}>
            Website made by Fabien CAYRE - 2022 - Åbo Akademi
          </span>
        </Container>
      </footer>
    </>
  );
};
