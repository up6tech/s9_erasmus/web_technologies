import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { FC, useState } from "react";
// import style from "@style/components/Header.module.css";
import style from "src/style/components/Header.module.css";
import { Button } from "./Button";
import { Container } from "./Container";
import {
  faBasketShopping,
  faMagnifyingGlass,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { useAuth, User } from "src/hooks/useAuth";
import { Stack } from "./Stack";
import { Tip } from "./Tip";

interface Props {
  enableBasket?: boolean;
  openBasket?: (() => void) | (() => Promise<void>);
  enableSearch?: boolean;
  updateSearch?: ((searchInput: string) => void) | undefined;
}

const AccountAction = () => {
  return (
    <div className={style.accountContainer}>
      <Button
        href="/signup"
        style={{
          fontSize: "16px",
        }}
      >
        Sign up
      </Button>
      <Button
        href="/login"
        style={{
          fontSize: "16px",
        }}
      >
        Sign in
      </Button>
    </div>
  );
};

interface AccountProps {
  username: string;
  enableBasket: boolean;
  openBasket?: (() => void) | (() => Promise<void>);
}

const LoginAccountAction: FC<AccountProps> = (props) => {
  const { logout, user } = useAuth();
  const cart = (user as User).basket;
  return (
    <>
      <Stack style={{ flexDirection: "column" }}>
        <Stack className={style.infos}>
          <span>{props.username}</span>
          {props.enableBasket && (
            <Button
              onClick={props.openBasket}
              style={{ backgroundColor: "transparent" }}
            >
              <Tip
                tag={cart.length > 9 ? "9+" : cart.length.toString()}
                disabled={cart.length == 0}
              >
                <FontAwesomeIcon
                  className={style.basketIcon}
                  icon={faBasketShopping}
                />
              </Tip>
            </Button>
          )}
        </Stack>
        <div className={style.actions}>
          <Button
            href="/account"
            style={{
              fontSize: "16px",
            }}
          >
            Change password
          </Button>
          <Button
            href="/myitems"
            style={{
              fontSize: "16px",
            }}
          >
            My Items
          </Button>
          <Button
            onClick={() => logout()}
            style={{
              fontSize: "16px",
            }}
          >
            Logout
          </Button>
        </div>
      </Stack>
    </>
  );
};

export const Header: FC<Props> = (props) => {
  const [search, setSearch] = useState<string>("");
  const { user, isAuth } = useAuth();
  const enableSearch = props.enableSearch || false;

  const inputSearchChange: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    setSearch(event.target.value);
    props.updateSearch?.(event.target.value);
  };

  return (
    <header className={style.header}>
      <Container
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          gap: "16px",
        }}
      >
        <div className={style.imageContainer}>
          <Link to={"/shop"}>
            <img
              alt="Website logo"
              src="http://localhost:8080/static/logo.png"
            />
          </Link>
        </div>
        <div className={style.mainContainer}>
          {/* Search bar */}
          {enableSearch && (
            <div className={style.searchbarContainer}>
              <FontAwesomeIcon
                className={style.searchIcon}
                icon={faMagnifyingGlass}
              />
              <input
                className={style.searchBar}
                onChange={inputSearchChange}
                value={search}
                placeholder="Search"
              />
            </div>
          )}
          {/* Account container */}
          {!isAuth() && <AccountAction />}
          {isAuth() && (
            <LoginAccountAction
              username={(user as User).username}
              enableBasket={props.enableBasket ?? true}
              openBasket={props.openBasket}
            />
          )}
        </div>
      </Container>
    </header>
  );
};
