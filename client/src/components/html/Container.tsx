import { FC, LegacyRef } from "react";
import style from "src/style/components/Container.module.css";

interface Props {
  children: React.ReactNode;
  style?: React.CSSProperties | undefined;
  className?: string | undefined;
}

const Container: FC<Props> = (props) => {
  return (
    <div
      style={{
        ...props.style,
      }}
      className={`${style.container} ${props.className || ""}`}
    >
      {props.children}
    </div>
  );
};

export { Container };
