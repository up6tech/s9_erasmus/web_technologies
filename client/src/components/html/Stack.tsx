import { FC } from "react";
import style from "src/style/components/Stack.module.css";

interface Props {
  style?: React.CSSProperties;
  children?: React.ReactNode;
  className?: string | undefined;
}

export const Stack: FC<Props> = (props) => {
  return (
    <div
      style={props.style}
      className={`${props?.className} ${style.container}`}
    >
      {props.children}
    </div>
  );
};
