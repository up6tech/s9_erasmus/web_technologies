import { FC } from "react";
import style from "src/style/components/Paper.module.css";

interface Props {
  children: React.ReactNode;
  style?: React.CSSProperties | undefined;
  className?: string | undefined;
}

export const Paper: FC<Props> = (props) => {
  return (
    <div
      style={{
        ...props.style,
      }}
      className={`${style.container} ${props.className || ""}`}
    >
      {props.children}
    </div>
  );
};
