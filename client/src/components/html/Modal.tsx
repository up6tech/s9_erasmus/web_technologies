import { FC } from "react";
import style from "src/style/components/Modal.module.css";

interface Props {
  open: boolean;
  children?: React.ReactNode;
}

export const Modal: FC<Props> = (props) => {
  return (
    <>
      <div className={`${style.modal} ${props.open ? style.open : ""}`}>
        <div className={style.modalBackground}></div>
        <div className={style.modalContainer}>{props.children}</div>
      </div>
    </>
  );
};
