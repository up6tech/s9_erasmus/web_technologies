import { FC, useEffect, useReducer, useState } from "react";
import { Stack } from "./html/Stack";
import style from "src/style/components/MyShopItems.module.css";
import { Button } from "./html/Button";
import { useAuth, User } from "src/hooks/useAuth";
import { DEFAULT_VALUE, NONE_PRODUCT, ShopItemStruct } from "./Types";
import { ShopItem } from "./ShopItems";
import { AddItem } from "./AddItem";
import { Alert } from "./indicators/Alert";
import { InteractiveShopItem } from "./InteractiveShopItem";
import { EditItem } from "./EditItem";

const NONE_USER = "none";
const url = "http://localhost:8080/api/user/items";
const deleteUrl = "http://localhost:8080/api/user/item";

interface Props {}
export const MyShopItems: FC<Props> = (props) => {
  const { user, isAuth } = useAuth();

  const [error, setError] = useState<string>(DEFAULT_VALUE);
  const [info, setInfo] = useState<string>(DEFAULT_VALUE);
  const [locked, setLocked] = useState<boolean>(false);

  const [saleItem, setSaleItem] = useState<ShopItemStruct[]>([]);
  const [soldItem, setSoldItem] = useState<ShopItemStruct[]>([]);
  const [boughtItem, setBoughtItem] = useState<ShopItemStruct[]>([]);
  const [val, forceUpdate] = useReducer((x) => x + 1, 0);

  const [add, setAdd] = useState<boolean>(false);
  const [edit, setEdit] = useState<boolean>(false);
  const [item, setItem] = useState<ShopItemStruct>(NONE_PRODUCT);

  const handleAdd = () => {
    setAdd(!add);
    setEdit(false);
    setItem(NONE_PRODUCT);
  };

  const handleEditItem = (item: ShopItemStruct) => {
    setAdd(false);
    setItem(item);
    setEdit(true);
  };

  const deleteItem = async (id: string) => {
    setLocked(true);
    const res = await fetch(deleteUrl, {
      method: "DELETE",
      body: JSON.stringify({ id: id }),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + (user as User).token,
      },
    });
    if (res.status == 200) {
      setInfo("Item deleted sucessfully!");
    } else {
      try {
        const err = await res.json();
        if (err.detail) {
          setTimeout(() => {
            setError(err.detail);
          }, 1500);
        }
      } catch (e) {}
    }
    forceUpdate();
    setLocked(false);
  };

  useEffect(() => {
    fetch(url, {
      method: "GET",
      headers: {
        Authorization: "Token " + (user as User).token,
      },
    })
      .then((res) => res.json())
      .then((products: ShopItemStruct[]) => {
        // saleItem: items with owner = current user and no buyer
        setSaleItem(
          products.filter(
            (i) => i.owner === (user as User).email && i.buyer === NONE_USER
          )
        );
        // soldItem: items with owner = current user and buyer
        setSoldItem(
          products.filter(
            (i) => i.owner === (user as User).email && i.buyer !== NONE_USER
          )
        );
        // boughtItem: items with buyer = current user
        setBoughtItem(products.filter((i) => i.buyer == (user as User).email));
        setTimeout(() => {
          setError(DEFAULT_VALUE);
          setInfo(DEFAULT_VALUE);
          setLocked(false);
        }, 1500);
      });
    return () => {};
  }, [val]);

  const toInteractiveComponent = (
    item: ShopItemStruct,
    index: number
  ): JSX.Element => {
    return (
      <ShopItem
        key={index}
        name={item.name}
        description={item.description}
        price={item.price}
        owner={item.owner}
        image={item.image_file_name}
        creation={item.creation}
      >
        <>
          <Button disabled={locked} onClick={() => deleteItem(item.id)}>
            Delete
          </Button>
          <Button disabled={locked} onClick={() => handleEditItem(item)}>
            Edit
          </Button>
        </>
      </ShopItem>
    );
  };

  const toComponent = (item: ShopItemStruct, index: number): JSX.Element => {
    return (
      <ShopItem
        key={index}
        name={item.name}
        description={item.description}
        price={item.price}
        owner={item.owner}
        image={item.image_file_name}
        creation={item.creation}
      />
    );
  };

  return (
    <>
      <Stack className={style.actions}>
        <Stack>
          {error !== DEFAULT_VALUE && <Alert type="error">{error}</Alert>}
          {info !== DEFAULT_VALUE && <Alert type="success">{info}</Alert>}
          <Stack style={{ flexDirection: "row" }}>
            <Button style={{ maxWidth: "150px" }} onClick={handleAdd}>
              Add item
            </Button>
          </Stack>
          {add && (
            <AddItem
              onSubmit={async () => {
                setInfo("Item added successfuly!");
                forceUpdate();
                setAdd(false);
              }}
            />
          )}
          {edit && (
            <EditItem
              onSubmit={async () => {
                setInfo("Item updated successfuly !");
                forceUpdate();
                setEdit(false);
                setItem(NONE_PRODUCT);
              }}
              item={item}
            />
          )}
        </Stack>
        <h3>Item to sale</h3>
        <div className={style.itemContainer}>
          {saleItem.length == 0 && <p>No items to sale</p>}
          {saleItem.map(toInteractiveComponent)}
        </div>
        <h3>Sold items</h3>
        <div className={style.itemContainer}>
          {soldItem.length == 0 && <p>No sold items</p>}
          {soldItem.map(toComponent)}
        </div>
        <h3>Bought items</h3>
        <div className={style.itemContainer}>
          {boughtItem.length == 0 && <p>No bought items</p>}
          {boughtItem.map(toComponent)}
        </div>
      </Stack>
    </>
  );
};
