import moment from "moment";
import { FC, useEffect, useState } from "react";
import style from "src/style/components/ShopItems.module.css";
import { range } from "src/Utils";
import { ApiData } from "./ApiData";
import { Paper } from "src/components/html/Paper";
import { Button } from "src/components/html/Button";
import { Stack } from "src/components/html/Stack";
import {
  DEFAULT_VALUE,
  ShopItemsResponseStruct,
  ShopItemStruct,
} from "./Types";
import { useNavigate } from "react-router-dom";
import { useAuth, User } from "src/hooks/useAuth";

interface ItemProps {
  name: string;
  description: string;
  price: number;
  owner: string;
  image: string;
  creation: string;
  children?: JSX.Element;
}

export const ShopItem: FC<ItemProps> = (props) => {
  return (
    <Paper>
      <div className={style.shopItem}>
        <div className={style.shopItemLeft}>
          <p>
            <strong>Name:</strong> {props.name}
          </p>
          <p>
            <strong>Description:</strong> {props.description}
          </p>
          <p>
            <strong>Price:</strong> {props.price}€
          </p>
          <p>
            <strong>Added by</strong> {props.owner}{" "}
            {moment(props.creation).fromNow()}
          </p>
        </div>
        <div className={style.shopItemRight}>
          <div
            className={style.shopItemImage}
            style={{
              backgroundImage: `url(http://localhost:8080/static/${props.image})`,
            }}
          />
        </div>
      </div>
      {props.children && (
        <Stack style={{ flexDirection: "row" }}>{props.children}</Stack>
      )}
    </Paper>
  );
};

interface Props {
  page?: number;
  changePage?: () => void;
  filter?: string | undefined;
}

export const ShopItems: FC<Props> = (props) => {
  const page = props.page ?? 0;
  const { user, isAuth, updateUser } = useAuth();
  const url = `/api/items?offset=${page}${
    props.filter ? "&search=" + encodeURIComponent(props.filter) : ""
  }`;

  const addToCart = (item: ShopItemStruct) => {
    const userData = user as User;
    userData.basket.push(item);
    updateUser(userData);
  };

  const isAddedToCart = (item: ShopItemStruct): boolean => {
    if (!isAuth || user === DEFAULT_VALUE) {
      return false;
    }
    const userData = user as User;
    if (
      userData.basket.filter((innerItem) => innerItem.id == item.id).length > 0
    )
      return true;
    return false;
  };

  const canBuy = (item: ShopItemStruct): boolean => {
    if (!isAuth || user === DEFAULT_VALUE) {
      return false;
    }
    const userData = user as User;
    if (userData.email === item.owner) return false;
    return true;
  };

  return (
    <>
      <ApiData
        url={url}
        render={function ({
          products,
          totalProducts,
          productPerPage,
        }: ShopItemsResponseStruct): JSX.Element {
          const pageCount = Math.ceil(totalProducts / productPerPage);
          return (
            <div className={style.container}>
              <p>
                Displaying {products.length} over {totalProducts} total products
              </p>
              <div className={style.itemsContainer}>
                {products.map((shopItem, index) => (
                  <ShopItem
                    key={index}
                    name={shopItem.name}
                    description={shopItem.description}
                    price={shopItem.price}
                    owner={shopItem.owner}
                    image={shopItem.image_file_name}
                    creation={shopItem.creation}
                  >
                    {canBuy(shopItem) ? (
                      !isAddedToCart(shopItem) ? (
                        <Button onClick={() => addToCart(shopItem)}>
                          Add to cart
                        </Button>
                      ) : (
                        <></>
                      )
                    ) : (
                      <></>
                    )}
                  </ShopItem>
                ))}
              </div>
              <Stack className={style.pagination}>
                {range(pageCount).map((v, index) => {
                  return (
                    <Button
                      className={style.paginationItem}
                      key={index}
                      href={`/shop/${v + 1}`}
                      disabled={v == page}
                    >
                      {v + 1}
                    </Button>
                  );
                })}
              </Stack>
            </div>
          );
        }}
      />
    </>
  );
};
