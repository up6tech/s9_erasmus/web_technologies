import { FC } from "react";
import { Navigate } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";

interface Props {
  children: React.ReactNode;
}

export const CheckNonAuthentification: FC<Props> = ({ children }) => {
  const { isAuth } = useAuth();
  if (isAuth()) {
    // user is authenticated
    return <Navigate to="/shop" />;
  }
  return <>{children}</>;
};

export const CheckAuthentification: FC<Props> = ({ children }) => {
  const { isAuth } = useAuth();
  if (!isAuth()) {
    // user is not authenticated
    return <Navigate to="/shop" />;
  }
  return <>{children}</>;
};
