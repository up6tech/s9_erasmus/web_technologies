#!/usr/bin/bash
main_folder=$(pwd)
# Install python requirements
cd $main_folder
pip install -r requirements.txt
# Compile React APP
cd $main_folder/client
npm install
npm run build
cd $main_folder
cp -r $main_folder/client/build $main_folder/server
# Make sure django migration is done
cd $main_folder/server 
python manage.py migrate