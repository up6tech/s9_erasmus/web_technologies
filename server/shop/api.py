from django.urls import path
from django.http import JsonResponse, HttpRequest
from django.views.decorators.http import require_http_methods
from .models import Product
from http import HTTPStatus


@require_http_methods(["GET"])
def items(request: HttpRequest):
    count = 10

    total_count = Product.objects.filter(buyer__isnull=True).count()
    offset_string = request.GET.get("offset", "0")
    search_string = request.GET.get("search", "__none__")
    if not offset_string.isnumeric():
        return JsonResponse({"error": "Offset is not a numeric value"},
                            status=HTTPStatus.BAD_REQUEST)

    offset = int(offset_string) * count

    if offset > total_count:
        return JsonResponse({"error": "Offset too large"},
                            status=HTTPStatus.BAD_REQUEST)

    result = Product.objects.filter(buyer__isnull=True)
    if search_string != "__none__":
        total_count = Product.objects.filter(name__contains=search_string,
                                             buyer__isnull=True).count()
        result = result.filter(name__contains=search_string)

    products = list(result.order_by('-creation')[offset:offset + count])
    products_as_json = list(product.map() for product in products)

    response_content = {
        "totalProducts": total_count,
        "productPerPage": count,
        "products": products_as_json
    }
    return JsonResponse(response_content)


@require_http_methods(["GET"])
def item(request: HttpRequest, item_id: int):
    try:
        p = Product.objects.get(id=int(item_id))
        return JsonResponse(p.map())
    except Product.DoesNotExist:
        return JsonResponse({"error": "Unknown product"},
                            status=HTTPStatus.NOT_FOUND)


urlpatterns = [path("items", items), path("item/<int:item_id>", item)]
