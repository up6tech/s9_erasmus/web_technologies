from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from .models import Product
from django.contrib.auth.models import User
from datetime import date, timedelta
from random import uniform

DB_USER_COUNT = 6
DB_USER_SELLERS = 3
DB_PRODUCT_PER_OWNER = 10


def shop_info() -> tuple[int, int, int]:
    """Return the shop main info for index

  Returns:
      tuple[int, int, int]: A tuple containing 3 int, product count, user count and the number of product
      added last week
  """
    # prepare filters
    today = date.today()
    inf_product = today - timedelta(days=7)
    inf_user = today - timedelta(days=30.1)

    # extract values from db
    product_count = Product.objects.count()
    user_count = User.objects.filter(is_active=True).count()
    added_last_week = Product.objects.filter(
        creation__range=[inf_product, today]).count()

    return (product_count, user_count, added_last_week)


def index(request: HttpRequest) -> HttpResponse:
    product_count, user_count, added_last_week = shop_info()
    return render(
        request,
        'dj_index.html',
        context={
            "page_title":
            "Home",
            "dynamic_info":
            f"Our page has {product_count}, with {added_last_week} items added last week, {user_count} active users"
        })


def refresh_database(request: HttpRequest) -> HttpResponse:
    # Empty databse
    Product.objects.all().delete()
    User.objects.all().delete()
    # Populate database
    for i in range(0, DB_USER_COUNT):
        u = User.objects.create_user(f"testuser{i}",
                                     f"testuser{i}@shop.aa",
                                     f"pass{i}",
                                     is_active=True)
        u.save()
        if i < DB_USER_SELLERS:
            for j in range(0, DB_PRODUCT_PER_OWNER):
                price = round(uniform(5, 60), 2)
                p = Product(
                    name=f"product {j}:{i}",
                    description=
                    f"This awesome product is completely real and owned by {u.email}, plus it only costs {price}€!!",
                    price=price,
                    image_file_name="default.png",
                    owner=u)
                p.save()

    product_count, user_count, added_last_week = shop_info()
    # Hydratation content page
    return render(
        request,
        'refresh.html',
        context={
            "page_title":
            "Home",
            "dynamic_info":
            f"Our page has {product_count}, with {added_last_week} items added last week, {user_count} active users"
        })


def shop_index(request: HttpRequest) -> HttpResponse:
    # TODO
    # Redirect to React APP
    return HttpResponse(content="todo")
