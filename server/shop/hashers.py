from django.contrib.auth.hashers import BasePasswordHasher
from django.utils.translation import gettext as _


class EmptyPasswordHasher(BasePasswordHasher):
    algorithm = 'empty'

    def verify(self, password, encoded):
        """Check if the given password is correct."""
        return password == encoded

    def encode(self, password, salt):
        return password

    def safe_summary(self, encoded):
        return {
            _('algorithm'): self.algorithm,
        }