from django.urls import path, include, re_path
from django.shortcuts import render
from . import views


def render_react(request):
    return render(request, "index.html")


urlpatterns = [
    path('', views.index),
    path('refresh_database', views.refresh_database),
    path('api/', include("shop.api")),
    path('api/auth/', include("shop.account_api")),
    path('api/user/', include("shop.product_api")),
    re_path(r"^$", render_react),
    re_path(r"^(?:.*)/?$", render_react),
]
