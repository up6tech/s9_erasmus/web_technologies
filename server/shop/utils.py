def lazy_str(string: str) -> str:
    """Return the string on call
  Args:
      string (string): The string value to be returned
  Returns:
      str: the value
  """
    return string
