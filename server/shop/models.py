from django.conf import settings
from django.db.models import Model as DjangoModel, EmailField
from django.db.models import CharField, TextField, DecimalField, ImageField, DateTimeField, ForeignKey, CASCADE
from django.utils.translation import gettext as _
from django.contrib.auth.models import User, AbstractUser

# class MyUser(AbstractUser):
#     USERNAME_FIELD = 'email'
#     email = EmailField(
#         _('email address'),
#         unique=True)  # changes email to unique and blank to false
#     REQUIRED_FIELDS = []


class Product(DjangoModel):
    name = CharField(max_length=50)
    description = TextField()
    price = DecimalField(decimal_places=2, max_digits=6)
    image_file_name = CharField(max_length=50, default=_("default.png"))
    creation = DateTimeField(auto_now_add=True)
    last_update = DateTimeField(auto_now_add=True)
    owner = ForeignKey(User, on_delete=CASCADE, related_name="owner")
    buyer = ForeignKey(User,
                       on_delete=CASCADE,
                       blank=True,
                       null=True,
                       related_name="buyer")

    def __str__(self):
        return f"{self.name} at {self.price}€ : {self.description}"

    def map(self):
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "price": self.price,
            "image_file_name": self.image_file_name,
            "creation": self.creation,
            "last_update": self.last_update,
            "owner": self.owner.email,
            "buyer": "none" if self.buyer is None else self.buyer.email
        }
