from django import forms


class SignUpForm(forms.Form):
    email = forms.CharField(label='Email', max_length=100)
    username = forms.CharField(label='username', max_length=100)
    password = forms.CharField(label='password', max_length=100)
    cpassword = forms.CharField(label='cpassword', max_length=100)


class SignInForm(forms.Form):
    email = forms.CharField(label='Email', max_length=100)
    password = forms.CharField(label='password', max_length=100)
