from rest_framework import serializers


class AddItemSerializer(serializers.Serializer):
    name = serializers.CharField(required=True, max_length=50)
    price = serializers.FloatField(required=True, min_value=0)
    description = serializers.CharField(required=True)


class BuyItemsSerializer(serializers.Serializer):
    items = serializers.ListField(child=serializers.IntegerField(min_value=0),
                                  required=True)


class UpdateItemSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    name = serializers.CharField(required=True, max_length=50)
    price = serializers.FloatField(required=True, min_value=0)
    description = serializers.CharField(required=True)


class RemoveItemSerialize(serializers.Serializer):
    id = serializers.IntegerField(required=True, min_value=0)
