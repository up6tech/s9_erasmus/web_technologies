from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from shop.models import Product
from shop.product_serializer import AddItemSerializer, RemoveItemSerialize, UpdateItemSerializer, BuyItemsSerializer
from django.db.models import Q
from http import HTTPStatus
from django.urls import path
from django.http import HttpRequest
from functools import reduce
from decimal import Decimal
import logging
# Get an instance of a logger
logger = logging.getLogger("django")


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def user_items(request, format=None):
    user = request.user
    products = Product.objects.filter(Q(owner=user)
                                      | Q(buyer=user)).order_by('-creation')
    products_as_json = list(product.map() for product in products)
    return Response(products_as_json)


@api_view(["POST", "DELETE", "PATCH"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def item(request, format=None):
    if request.method == "POST":
        serializer = AddItemSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        user = request.user
        product = Product(name=data["name"],
                          description=data["description"],
                          price=data["price"],
                          image_file_name="default.png",
                          owner=user)

        product.save()
        product_name = data["name"]

        return Response({"ok": f"Item {product_name} added successfuly"})
    elif request.method == "DELETE":
        # delete
        serializer = RemoveItemSerialize(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        product_id = data["id"]

        user = request.user
        product = Product.objects.get(id=product_id, owner=user)
        if product is None:
            return Response(
                {"error": f"Product with id {product_id} not found"},
                status=HTTPStatus.NOT_FOUND)

        product.delete()
        return Response({"ok": "Product deleted!"})
    elif request.method == "PATCH":
        serializer = UpdateItemSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        product_id = data["id"]
        user = request.user

        product = Product.objects.filter(id=product_id)
        if product is None:
            return Response(
                {"error": f"Product with id {product_id} not found"},
                status=HTTPStatus.NOT_FOUND)

        product.update(
            name=data["name"],
            description=data["description"],
            price=data["price"],
        )
        return Response({"ok": "Product updated!"})


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def payment(request: HttpRequest):
    serializer = BuyItemsSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    user = request.user
    data = serializer.validated_data

    Product.objects.filter(id__in=data["items"]).update(buyer=user)

    products = list(Product.objects.filter(id__in=data["items"]))
    total_cost = reduce(lambda acc, product: acc + product.price, products,
                        Decimal(0.0))

    message = f"""
        Dear {user.username},
        
        thanks you for purchasing {len(products)} products !
        Here the receipt, for a total of {total_cost} €!
        
        Kind regards."""

    logger.info("=========== EMAIL AS BACKEND ===========")
    logger.info(message)

    return Response({"ok": "Payment validated !"})


urlpatterns = [
    path("items", user_items),
    path("item", item),
    path("payment", payment)
]
