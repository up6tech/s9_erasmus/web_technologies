from django.urls import path
from django.http import JsonResponse, HttpRequest, HttpResponse
from .models import Product
from django.views.decorators.http import require_http_methods
from http import HTTPStatus
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from shop.account_serializer import RegisterUserSerializer, UserSerializer, obtain_auth_token_email, ChangePasswordSerializer
from django.db.models import Q
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class SignupView(APIView):
    """
    Create a new user with token
    """

    def post(self, request):
        serializer = RegisterUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data

        if User.objects.filter(
                Q(email=data["email"])
                | Q(username=data["username"])).exists():
            return JsonResponse(
                {
                    "error": "This email or username is already used!",
                },
                status=HTTPStatus.BAD_REQUEST)

        user = User.objects.create_user(username=data["username"],
                                        email=data["email"],
                                        password=data["password"])

        Token.objects.create(user=user)
        user_serializer = UserSerializer(user)
        return Response(user_serializer.data)


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def user_info(request, format=None):
    content = {
        'email': str(request.user.email),
        'username': str(request.user.username),
    }
    return Response(content)


@require_http_methods(["POST"])
def signout(request: HttpRequest):
    return JsonResponse({"error": "logout NotImplemented"})


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def change_password(request, format=None):

    serializer = ChangePasswordSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    data = serializer.validated_data

    user: User = request.user
    if not user.check_password(data["oldPassword"]):
        return Response({"error": "Wrong old password provided"},
                        status=HTTPStatus.BAD_REQUEST)

    user.set_password(data["password"])
    user.save()

    return Response({"ok": "Password changed successfuly !"})


urlpatterns = [
    path("signup", SignupView.as_view()),
    path("signin", obtain_auth_token_email, name='token_obtain_pair'),
    path("signin/refresh", TokenRefreshView.as_view(), name='token_refresh'),
    path("signout", signout),
    path("userinfo", user_info),
    path("changepassword", change_password)
]
