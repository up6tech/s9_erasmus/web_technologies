# Web Shop Project

### Personal info

CAYRE Fabien - Spring 2022
fabien.cayre@abo.fi
contact@fabiencayre.fr

## Implemented Requirements

### Mandatory

- [**1**] Site architecture
- [**2**] Automatic DB population
- [**3**] Browse (Pagination)
- [**4**] Search
- [**5**] Create account (Django Restframework Token)
- [**6**] Login (Django Restframework Token on server, AuthContext & Hooks on client)
- [**7**] Add item
- [**8**] Add to cart
- [**9**] Remove from cart
- [**10**] Pay (MANDATORY)
  - Email send to console
- [**11**] Routing
- [**12**] Edit account
  - Change password
- [**13**] Display inventory
- [**14**] Edit items (only ones on sale)
  - You can delete them too
- [**16**] Security (React ContextAPI authentification check )
- [**17**] Usability (tried my best!)

#### Technical

- [**18**] Django as backend (+Django restframework)
- [**19**] Frontend: use reactJS (+ a bunch of lib, superpowered by Typescript )
- [**20**] Web app working on latest chrome, chromium and firefox browsers.
- [**21**] Project folder
  - Frontend in /client & backend in /server
  - Quick launch the entire project with (continue to execute commands):
    - ./build.sh
    - Docker (3mins build time at first)

### Optional

- [**11**] Routing (react-router-dom + auth check)
- [**12**] Edit Account (change password)

---

# Requirements

- NodeJS 18.8+
- NPM XX.X+

# Installation

Server side:

```shell
. .venv/bin/activate
pip install -r requirements.txt
```

Client side:

```shell
cd client
npm install
```

# Launch script

At the root of the project:
**! Make sure you are in venv !**

```shell
./build.sh
cd server && python manage.py runserver 8080
```

# Docker

Build and launch standalone container

At the root of the project:

```shell
docker build . -t webproject \
  && docker run --network=host webproject
```
